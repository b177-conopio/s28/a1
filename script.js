// Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.log(json));

// Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json));

// Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'GET'
})
.then((response) => response.json())
.then((json) => console.log(
    `The item ${json.title} has a status of ${json.completed}`
    ));

// Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST', 
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		title: 'Created To Do List',
		completed: false
	}),
})
.then((response) => response.json())
.then((json) => console.log(json));

//  Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'Updated To Do List Item',
		userId: 1,
		dateCompleted: 'Pending',
		description: 'To update the my to do list with a different data structure',
		status: 'Pending'
	}),
})
.then((response) => response.json())
.then((json) => console.log(json));

// Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		userId: 1,
		dateCompleted: '07/09/21',
		status: 'Complete'
	}),
})
.then((response) => response.json())
.then((json) => console.log(json));

// Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
})